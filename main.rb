#Esse é um método que serve para iniciar interações com o garbage collector, incluindo o monitoramento
GC.start 

# Abre o arquivo
file = File.open("dataset.txt")

# Salva todos os nomes em um array separando por linha, aqui que o codigo começa a pesar
nomes = file.readlines.map(&:chomp) 

qtde_nomes_ditto = 0 # Quantidade de repetidos

# Pega o tempo inicial
tempo_ini = Time.now 

# Calcula a quantidade de nomes repetidos 
# subtraindo a quantidade de nomes unicos da quantidade total de nomes
qtde_nomes_ditto = nomes.count - nomes.uniq.count

# Pega o tempo final
tempo_fin = Time.now 

puts ("Tempo decorrido: #{tempo_fin - tempo_ini}") # Mostra o tempo decorrido
puts ("Quantidade de nomes Repetidos: #{qtde_nomes_ditto}") # Mostra a quantidade de repitidos
puts ("GC Stats: ")
require 'pp'
pp GC.stat
