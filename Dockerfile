FROM ubuntu
RUN apt-get upgrade && apt-get update && apt-get install -y ruby-full && gem install ffaker
COPY main.rb /usr/src/main.rb 
WORKDIR /usr/src/ 
CMD ruby main.rb