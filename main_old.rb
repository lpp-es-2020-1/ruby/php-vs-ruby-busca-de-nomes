require 'ffaker' # Carregar ffaker para gerar a entrada

nomes = []
nomes_unicos = []
qtde_nomes = 100000 # Quantidade de nomes

tempo_ini = Time.now # Pega o tempo inicial

for i in 1..qtde_nomes do
    nomes[i] = FFaker::Name.name
end

nomes_unicos = nomes.uniq # Separa os nomes únicos

tempo_fin = Time.now # Pega o tempo final
tempo_decorrido = tempo_fin - tempo_ini # Calcula o tempo decorrido

puts (tempo_decorrido) # Mostra o tempo decorrido
puts (qtde_nomes - nomes_unicos.count) # Mostra a quantidade de repitidos
